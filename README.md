# Git emojis hook

Deux hooks git pour permettre l'utilisation des emojis à l'intérieur des messages de commit.

## Utilisation
Pour chaque projet, ajoutez les deux fichiers, `commit-msg` et `prepare-commit-msg` dans le répertoire `.git/hooks`.
Pour ce faire utilisez cette ligne de commande à la racine du projet:

```
cd .git/hooks/ && curl -O https://bitbucket.org/rosqoe/git-emojis-hook/raw/master/commit-msg && curl -O https://bitbucket.org/rosqoe/git-emojis-hook/raw/master/prepare-commit-msg && chmod +x * || exit 0
```

## Type
* **revert**: Retour sur un commit antérieur.
* **build**: Modifications qui affectent le système ou les dépendances externes.
* **ci**: Modifications des fichiers et scripts de configuration CI.
* **docs**: Modifications concernant une quelconque documentation.
* **feat**: Création d'une nouvelle fonctionnalité.
* **fix**: Correction d'un bogue.
* **perf**: Modifications de code qui améliorent les performances.
* **refactor**: Modifications de code qui ne corrigent pas de bogue ni n'ajoutent de fonctionnalité.
* **style**: Modifications concernant la partie esthétique.
* **test**: Ajout de tests manquants ou correction de tests existants.

## Syntaxe
Voici les types, leurs codes respectifs et les emojis correspondants:

* **revert**: `:revert:` • ⏳
* **build**: `:build:` • 📦
* **ci**: `:ci:` • 🤖
* **docs**: `:docs:` • 📖
* **feat**: `:feat:` • 🌟
* **fix**: `:fix:` • 🚑
* **perf**: `:perf:` • ⚡
* **refactor**: `:refactor:` • 🚧
* **style**: `:style:` • 💄
* **test**: `:test:` • ✅

En plus de ceux-ci, il y a le `:tada:` 🎉, à utiliser pour le premier commit !

Les emojis fonctionnent pratiquement partout tant que l'unicode est pris en charge.

## Exemple

```
🚑 remove buggy function that allowed to login w/o passwd
```
